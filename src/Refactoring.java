package ru;

public class Refactoring {
    public static void main(String[] args) {

        int one_i = 5;
        print_i(one_i);

        int two_i = -5;
        print_i(two_i);

        double one_d = 2.5;
        print_d(one_d);

        double two_d = -2.5;
        print_d(two_d);
    }

    private static void print_d(double two_d) {
        String s_Result = "";
        long number_Bits = Double.doubleToLongBits(two_d);

        s_Result = Long.toBinaryString(number_Bits);
        System.out.println("Представление вещественного числа в формате чисел с плавающей точкой");

        System.out.format("Число: %5.2f\n", two_d);
        System.out.println("Формат чисел с плавающей точкой:");

        //ведущий ноль заботливо сокращен системой, поэтому его нужно восстановить
        System.out.println(two_d > 0 ? "0" + s_Result : s_Result);
        System.out.println("");
    }

    private static void print_i(int two_i) {
        String int_Bits2 = Integer.toBinaryString(two_i);
        System.out.println("Число: " + two_i);
        System.out.println("Разряды числа: " + int_Bits2);
        System.out.println("");
    }
}
